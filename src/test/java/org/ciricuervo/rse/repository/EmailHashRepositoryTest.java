/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.repository;

import static org.junit.Assert.*;

import java.time.LocalDateTime;

import org.ciricuervo.rse.domain.EmailHash;
import org.ciricuervo.rse.domain.type.EmailHashType;
import org.ciricuervo.rse.service.EmailHashService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * {@link EmailHashRepository} unit test.
 * 
 * @author ciri-cuervo
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@Transactional
public class EmailHashRepositoryTest {

	private static final LocalDateTime NOW = LocalDateTime.now();
	private static final LocalDateTime TOMORROW = NOW.plusDays(1);

	@Autowired
	private EmailHashRepository emailHashRepository;
	@Autowired
	private EmailHashService emailHashService;

	private EmailHash emailHash;
	private EmailHash emailHashExpired;

	@Before
	public void setUp() throws Exception {
		emailHash = new EmailHash();
		emailHash.setEmail("testuser@email.com");
		emailHash.setHash(emailHashService.createHash());
		emailHash.setType(EmailHashType.ACCOUNT_ACT);
		emailHash.setActive(true);
		emailHash.setExpire(TOMORROW);

		emailHashExpired = new EmailHash();
		emailHashExpired.setEmail("testuser@email.com");
		emailHashExpired.setHash(emailHashService.createHash());
		emailHashExpired.setType(EmailHashType.ACCOUNT_ACT);
		emailHashExpired.setActive(true);
		emailHashExpired.setExpire(NOW);
	}

	@Test
	public void testCreateEmailHash() {
		emailHash = emailHashRepository.save(emailHash);
		EmailHash actual = emailHashRepository.findByHash(emailHash.getHash()).orElse(null);

		assertNotNull(actual);
		assertEquals(emailHash.getId(), actual.getId());
		assertEquals(emailHash.getEmail(), actual.getEmail());
		assertEquals(emailHash.getHash(), actual.getHash());
		assertNotNull(actual.getCreatedDate());
		assertNotNull(actual.getLastModifiedDate());
	}

	@Test
	public void testFindByHashAndActiveTrue() {
		emailHash = emailHashRepository.save(emailHash);
		emailHashExpired = emailHashRepository.save(emailHashExpired);

		EmailHash actual = emailHashRepository.findByHashAndActiveTrue(emailHash.getHash()).orElse(null);
		assertNotNull(actual);
		assertEquals(emailHash, actual);

		actual = emailHashRepository.findByHashAndActiveTrue(emailHashExpired.getHash()).orElse(null);
		assertNotNull(actual);
		assertEquals(emailHashExpired, actual);

		emailHash.setActive(false);
		emailHash = emailHashRepository.save(emailHash);

		actual = emailHashRepository.findByHashAndActiveTrue(emailHash.getHash()).orElse(null);
		assertNull(actual);
	}

	@Test
	public void testFindByEmailAndTypeAndActiveTrue() {
		emailHash = emailHashRepository.save(emailHash);
		emailHashExpired.setActive(false);
		emailHashExpired = emailHashRepository.save(emailHashExpired);

		EmailHash actual =
				emailHashRepository.findByEmailAndTypeAndActiveTrue(emailHash.getEmail(), EmailHashType.ACCOUNT_ACT).orElse(null);
		assertNotNull(actual);
		assertEquals(emailHash, actual);

		emailHash.setActive(false);
		emailHash = emailHashRepository.save(emailHash);

		actual = emailHashRepository.findByEmailAndTypeAndActiveTrue(emailHash.getEmail(), EmailHashType.ACCOUNT_ACT).orElse(null);
		assertNull(actual);
	}

	@Test
	public void testDeactivateByEmail() {
		emailHash = emailHashRepository.save(emailHash);
		emailHashExpired = emailHashRepository.save(emailHashExpired);

		assertTrue(emailHash.getActive());
		assertTrue(emailHashExpired.getActive());

		Integer rows = emailHashRepository.deactivateByEmail(emailHash.getEmail());
		assertEquals(Integer.valueOf(2), rows);

		EmailHash actual = emailHashRepository.findByHash(emailHash.getHash()).orElse(null);
		assertNotNull(actual);
		assertEquals(emailHash, actual);
		assertFalse(actual.getActive());

		actual = emailHashRepository.findByHash(emailHashExpired.getHash()).orElse(null);
		assertNotNull(actual);
		assertEquals(emailHashExpired, actual);
		assertFalse(actual.getActive());
	}

	@Test
	public void testDeactivateByExpired() {
		emailHash = emailHashRepository.save(emailHash);
		emailHashExpired = emailHashRepository.save(emailHashExpired);

		assertTrue(emailHash.getActive());
		assertTrue(emailHashExpired.getActive());

		Integer rows = emailHashRepository.deactivateByExpired();
		assertEquals(Integer.valueOf(1), rows);

		EmailHash actual = emailHashRepository.findByHash(emailHash.getHash()).orElse(null);
		assertNotNull(actual);
		assertEquals(emailHash, actual);
		assertTrue(actual.getActive());

		actual = emailHashRepository.findByHash(emailHashExpired.getHash()).orElse(null);
		assertNotNull(actual);
		assertEquals(emailHashExpired, actual);
		assertFalse(actual.getActive());
	}

	@Test
	public void testDeleteByActiveFalse() {
		emailHash.setActive(false);
		emailHash = emailHashRepository.save(emailHash);
		Integer rows = emailHashRepository.deleteByActiveFalse();
		EmailHash actual = emailHashRepository.findByHash(emailHash.getHash()).orElse(null);

		assertNull(actual);
		assertEquals(Integer.valueOf(1), rows);
	}

}
