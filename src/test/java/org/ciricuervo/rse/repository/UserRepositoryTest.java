/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.repository;

import static org.junit.Assert.*;

import java.time.LocalDateTime;

import org.ciricuervo.rse.domain.User;
import org.ciricuervo.rse.domain.type.AuthorityType;
import org.ciricuervo.rse.domain.type.UserStatusType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * {@link UserRepository} unit test.
 * 
 * @author ciri-cuervo
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@Transactional
public class UserRepositoryTest {

	@Autowired
	private UserRepository userRepository;

	private User user;

	@Before
	public void setUp() throws Exception {
		user = new User();
		user.setUsername("testuser");
		user.setPassword("testpassword");
		user.setEmail("testuser@email.com");
		user.setStatus(UserStatusType.ACTIVE);
		user.addAuthority(AuthorityType.ROLE_USER);
	}

	@Test
	public void testCreateUser() {
		user = userRepository.save(user);
		User actual = userRepository.findByUsername(user.getUsername()).orElse(null);

		assertNotNull(actual);
		assertEquals(user.getId(), actual.getId());
		assertEquals(user.getEmail(), actual.getEmail());
		assertEquals(1, actual.getAuthorities().size());
		assertTrue(actual.hasAuthority(AuthorityType.ROLE_USER));
		assertNotNull(actual.getCreatedDate());
		assertNotNull(actual.getLastModifiedDate());
	}

	@Test
	public void testDeleteUser() {
		user = userRepository.save(user);
		userRepository.delete(user);
		User actual = userRepository.findByUsername(user.getUsername()).orElse(null);

		assertNull(actual);
	}

	@Test
	public void testEditRoles() {
		user.addAuthority(AuthorityType.ROLE_ADMIN);
		user = userRepository.save(user);

		assertEquals(2, user.getAuthorities().size());
		assertTrue(user.hasAuthority(AuthorityType.ROLE_ADMIN));

		user.removeAuthority(AuthorityType.ROLE_ADMIN);
		user = userRepository.save(user);

		assertEquals(1, user.getAuthorities().size());
		assertFalse(user.hasAuthority(AuthorityType.ROLE_ADMIN));
	}

	@Test
	public void testDeleteByWaitingActivation() {
		user.setStatus(UserStatusType.WAITING_ACTIVATION);
		user = userRepository.save(user);
		Integer rows = userRepository.deleteByWaitingActivation(LocalDateTime.now());
		User actual = userRepository.findByUsername(user.getUsername()).orElse(null);

		assertNull(actual);
		assertEquals(Integer.valueOf(1), rows);
	}

}
