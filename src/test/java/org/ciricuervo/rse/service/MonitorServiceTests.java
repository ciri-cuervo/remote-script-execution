/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.service;

import static org.junit.Assert.*;

import java.net.UnknownHostException;
import java.util.Set;

import org.ciricuervo.rse.service.remoteexecution.MonitorService;
import org.ciricuervo.rse.service.remoteexecution.RemoteExecutionException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.Timed;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.ResourceAccessException;

import lombok.extern.slf4j.Slf4j;

/**
 * {@link MonitorService} unit test.
 * 
 * @author ciri-cuervo
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@Slf4j
public class MonitorServiceTests {

	@Autowired
	private MonitorService monitorService;

	@Test
	@WithMockUser(username = "user", roles = "USER")
	public void testMonitorServer() throws Exception {
		String output = monitorService.monitorServer(1L);
		log.debug("Test output:\n{}", output);
		assertNotEquals("the output must not be empty", "", output.trim());
	}

	@Test(expected = UnknownHostException.class)
	@WithMockUser(username = "user", roles = "USER")
	@Timed(millis = 15000)
	public void testMonitorServerInvalidServer() throws Exception {
		try {
			log.debug("Test output:\n{}", monitorService.monitorServer(2L));
		} catch (RemoteExecutionException e) {
			if (e.getCause() != null) {
				throw (Exception) e.getCause().getCause();
			} else {
				throw (Exception) e.getCause();
			}
		}
	}

	@Test(expected = AccessDeniedException.class)
	@WithMockUser(username = "user", roles = "ANONYMOUS")
	public void testMonitorServerWithoutAuthorization() throws Exception {
		log.debug("Test output:\n{}", monitorService.monitorServer(1L));
	}

	@Test(expected = ResourceAccessException.class)
	@WithMockUser(username = "user", roles = "USER")
	public void testMonitorServerNonExisting() throws Exception {
		log.debug("Test output:\n{}", monitorService.monitorServer(-1L));
	}

	@Test
	@WithMockUser(username = "user", roles = "USER")
	public void testGetRunningProcesses() throws Exception {
		Set<String> pids = monitorService.getRunningProcesses(1L);
		log.debug("Test PIDs: {}", pids);
		assertFalse("the pids must not be empty", pids.isEmpty());
	}

}
