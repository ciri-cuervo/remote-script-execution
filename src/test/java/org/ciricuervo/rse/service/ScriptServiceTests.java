/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.service;

import org.ciricuervo.rse.service.remoteexecution.ScriptService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.ResourceAccessException;

/**
 * {@link ScriptService} unit test.
 * 
 * @author ciri-cuervo
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class ScriptServiceTests {

	@Autowired
	private ScriptService scriptService;

	@Test
	@WithMockUser(username = "user", roles = "USER")
	public void testRunScript() throws Exception {
		scriptService.runScript(1L);
	}

	@Test(expected = AccessDeniedException.class)
	@WithMockUser(username = "user", roles = "ANONYMOUS")
	public void testRunScriptWithoutAuthorization() throws Exception {
		scriptService.runScript(1L);
	}

	@Test(expected = ResourceAccessException.class)
	@WithMockUser(username = "user", roles = "USER")
	public void testRunScriptNonExisting() throws Exception {
		scriptService.runScript(-1L);
	}

}
