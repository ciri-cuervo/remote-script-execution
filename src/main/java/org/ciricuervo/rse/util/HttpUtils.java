/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.util.StringUtils;

import lombok.experimental.UtilityClass;

/**
 * Helper utility class with constants and static methods for general use.
 * 
 * @author ciri-cuervo
 */
@UtilityClass
public class HttpUtils {

	/**
	 * Returns the base URL of the application: {@code http[s]://domain.com[:8080][/contextpath]}
	 */
	public static String getBaseUrl(HttpServletRequest request) {
		String requestUrl = request.getRequestURL().toString();
		String requestUri = request.getRequestURI();
		return requestUrl.substring(0, requestUrl.length() - requestUri.length());
	}

	/**
	 * Obtain the request URI without the context path and with the query string appended.
	 * 
	 * @param request the current request
	 * @return the path requested with the query string appended
	 */
	public static String getRequestPathWithQuery(HttpServletRequest request) {
		String contextPath = request.getContextPath();
		String requestPath = request.getRequestURI().substring(contextPath.length());
		return appendCurrentQueryParams(requestPath, request);
	}

	/**
	 * Append the query string of the current request to the target URL.
	 * 
	 * @param targetUrl the String to append the properties to
	 * @param request the current request
	 * @return the target URL with the query string appended
	 */
	public static String appendCurrentQueryParams(String targetUrl, HttpServletRequest request) {
		String query = request.getQueryString();
		if (StringUtils.hasText(query)) {
			return targetUrl + '?' + query;
		} else {
			return targetUrl;
		}
	}

	/**
	 * .
	 */
	public static String appendParameterToQuery(String query, String name, Object value, String encoding)
			throws UnsupportedEncodingException {
		StringBuilder sb = new StringBuilder(query);
		sb.append(query.contains("?") ? '&' : '?');
		sb.append(URLEncoder.encode(name, encoding));
		sb.append('=');
		sb.append(URLEncoder.encode(String.valueOf(value), encoding));
		return sb.toString();
	}

	/**
	 * .
	 */
	public static void sendTemporaryRedirect(HttpServletRequest request, HttpServletResponse response, String redirectUrl) {
		String encodedRedirectUrl = response.encodeRedirectURL(request.getContextPath() + redirectUrl);
		response.setStatus(HttpServletResponse.SC_TEMPORARY_REDIRECT);
		response.setHeader("Location", encodedRedirectUrl);
	}

}
