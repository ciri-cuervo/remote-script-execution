/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.util;

import lombok.experimental.UtilityClass;

/**
 * .
 * 
 * @author ciri-cuervo
 */
@UtilityClass
public class Regexp {

	public static final String DOMAIN = "((?!-)[A-Za-z0-9-]{1,63}(?<!-)\\.)+[A-Za-z]{2,6}";
	public static final String DOMAIN_PATTERN = "\\*|(?!.*\\*{2})((?!-)[A-Za-z0-9\\-\\*\\?]{1,63}(?<!-)\\.)+(\\*|[A-Za-z\\*\\?]{2,6})";

	public static final String USERNAME = "[a-z0-9-]{4,32}";
	public static final String EMAIL = "(?!.{255})[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + DOMAIN;
	public static final String SIMPLE_EMAIL = "[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@([A-Za-z0-9-]{1,63}\\.)+[A-Za-z]{2,6}";
	public static final String PASSWORD = "(?=(.*[A-Za-z]){1,})(?=(.*[0-9]){1,}).{6,32}";
	public static final String HASH = "[0-9a-zA-Z]{8,256}";

}
