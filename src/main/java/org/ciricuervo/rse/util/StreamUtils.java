/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.util;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

import lombok.experimental.UtilityClass;

/**
 * Complementary features for Java streams.
 * 
 * @author ciri-cuervo
 */
@UtilityClass
public class StreamUtils {

	/**
	 * Utility method to get distinct items from a stream based on a key.
	 */
	public static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) {
		Map<Object, Boolean> seen = new ConcurrentHashMap<>();
		return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
	}

	/**
	 * Turns an Optional&lt;T&gt; into a Stream&lt;T&gt; of length zero or one depending upon whether a value is present.
	 * <p>
	 * TODO: {@code Optional.stream()} has been added to JDK 9.
	 * </p>
	 */
	public static <T> Stream<T> streamopt(Optional<T> opt) {
		return opt.isPresent() ? Stream.of(opt.get()) : Stream.empty();
	}

}
