/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.util;

import java.util.Optional;

import org.ciricuervo.rse.ApplicationContextProvider;
import org.ciricuervo.rse.domain.User;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.RememberMeAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.util.Assert;

import lombok.experimental.UtilityClass;

/**
 * Helper utility class for security.
 * 
 * @author ciri-cuervo
 */
@UtilityClass
public class SecurityUtils {

	public static final String SYSTEM_USERNAME = "system";

	/**
	 * Returns the authenticated {@link User}.
	 */
	public static Optional<User> getAuthenticatedUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null && authentication.getPrincipal() instanceof User) {
			return Optional.of((User) authentication.getPrincipal());
		}
		return Optional.empty();
	}

	public static boolean isAnonymous() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return authentication instanceof AnonymousAuthenticationToken;
	}

	public static boolean isRememberMeAuthenticated() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return authentication instanceof RememberMeAuthenticationToken;
	}

	public static boolean isAuthenticated() {
		return !isAnonymous();
	}

	public static boolean isFullyAuthenticated() {
		return !isAnonymous() && !isRememberMeAuthenticated();
	}

	/**
	 * Ensures that the authentication principal is anonymous.
	 * 
	 * @throws AccessDeniedException when the authentication principal is not anonymous
	 */
	public static void assertAnonymous() {
		if (!isAnonymous()) {
			throw new AccessDeniedException("Sorry, you are not allowed to access here.");
		}
	}

	/**
	 * Ensures that the authentication principal is remember-me authenticated.
	 * 
	 * @throws AccessDeniedException when the authentication principal is not remember-me authenticated
	 */
	public static void assertRememberMeAuthenticated() {
		if (!isRememberMeAuthenticated()) {
			throw new AccessDeniedException("Sorry, you are not allowed to access here.");
		}
	}

	/**
	 * Ensures that the authentication principal is authenticated.
	 * 
	 * @throws AccessDeniedException when the authentication principal is not authenticated
	 */
	public static void assertAuthenticated() {
		if (!isAuthenticated()) {
			throw new AccessDeniedException("Sorry, you are not allowed to access here.");
		}
	}

	/**
	 * Ensures that the authentication principal is fully authenticated.
	 * 
	 * @throws AccessDeniedException when the authentication principal is not fully authenticated
	 */
	public static void assertFullyAuthenticated() {
		if (!isFullyAuthenticated()) {
			throw new AccessDeniedException("Sorry, you are not allowed to access here.");
		}
	}

	/**
	 * Logs-in system user.
	 */
	public static void loginSystemUser() {
		Assert.isNull(SecurityContextHolder.getContext().getAuthentication(), "Logged user can't grant SYSTEM privileges.");
		UserDetailsService userDetailsService = ApplicationContextProvider.ctx.getBean(UserDetailsService.class);
		UserDetails user = userDetailsService.loadUserByUsername(SYSTEM_USERNAME);
		Authentication auth = new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword(), user.getAuthorities());
		SecurityContextHolder.getContext().setAuthentication(auth);
	}

	/**
	 * Logs-out system user.
	 */
	public static void logoutSystemUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null) {
			Assert.isTrue(SYSTEM_USERNAME.equals(authentication.getName()), "Logged user is not system.");
			SecurityContextHolder.getContext().setAuthentication(null);
		}
	}

}
