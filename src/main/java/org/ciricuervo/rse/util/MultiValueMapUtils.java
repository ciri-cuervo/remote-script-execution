/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.util;

import static java.util.stream.Collectors.*;

import java.util.Collection;
import java.util.Map;
import java.util.function.Supplier;

import lombok.experimental.UtilityClass;

/**
 * {@code MultiValueMap} utility class.
 * 
 * @author ciri-cuervo
 */
@UtilityClass
public class MultiValueMapUtils {

	/**
	 * Adds an element to the {@code MultiValueMap}.
	 */
	public static <K, V, C extends Collection<V>> boolean add(Map<K, C> map, K key, V value, Supplier<? extends C> factory) {
		C values = map.get(key);
		if (values == null) {
			map.put(key, values = factory.get());
		}
		return values.add(value);
	}

	/**
	 * Removes the first occurrence of the specified element from the {@code MultiValueMap}.
	 */
	public static <K, V> boolean remove(Map<K, ? extends Collection<V>> map, K key, V value) {
		Collection<V> values = map.get(key);
		boolean removed = false;
		if (values != null && (removed = values.remove(value)) && values.isEmpty()) {
			map.remove(key);
		}
		return removed;
	}

	/**
	 * Returns {@code true} if the {@code MultiValueMap} maps the specified key to the specified value.
	 */
	public static <K, V> boolean contains(Map<K, ? extends Collection<V>> map, K key, V value) {
		Collection<V> values = map.get(key);
		return values != null && values.contains(value);
	}

	/**
	 * Returns all the values in the {@code MultiValueMap}.
	 */
	public static <K, V, C extends Collection<V>> C values(Map<K, ? extends Collection<V>> map, Supplier<? extends C> factory) {
		return map.values().stream().flatMap(Collection::stream).collect(toCollection(factory));
	}

}
