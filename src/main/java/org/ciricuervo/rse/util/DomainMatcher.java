/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.util;

import java.util.regex.Pattern;

/**
 * .
 * 
 * @author ciri-cuervo
 */
public class DomainMatcher {

	protected static final Pattern DOMAIN_PATTERN = Pattern.compile(Regexp.DOMAIN_PATTERN);
	protected static final Pattern DOMAIN = Pattern.compile(Regexp.DOMAIN);

	public static boolean isDomainPattern(String pattern) {
		return (pattern == null) ? false : DOMAIN_PATTERN.matcher(pattern).matches();
	}

	public static boolean isDomain(String domain) {
		return (domain == null) ? false : DOMAIN.matcher(domain).matches();
	}

	/**
	 * .
	 */
	public static boolean match(String pattern, String domain) {
		if ("*".equals(pattern)) {
			return isDomain(domain);
		}
		if (isDomainPattern(pattern) && isDomain(domain)) {
			return domain.matches(toNativePattern(pattern));
		}
		return false;
	}

	private static String toNativePattern(String pattern) {
		pattern = pattern.replace(".", "\\.");
		pattern = pattern.replace("?", "[^\\.]");
		pattern = pattern.replace("*", "[^\\.]*?");
		return pattern;
	}

}
