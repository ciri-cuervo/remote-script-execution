/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * Used to create a deployable war file.
 * 
 * @author ciri-cuervo
 */
public class ServletInitializer extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(Application.class);
	}

}
