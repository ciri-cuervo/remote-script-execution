/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.config;

import java.util.Locale;

import org.ciricuervo.rse.security.AnonymousRestrictedHandlerInterceptor;
import org.ciricuervo.rse.security.NegatedRequestCacheClearInterceptor;
import org.ciricuervo.rse.util.WebConstants.Views;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

/**
 * Spring Web MVC configuration.
 * 
 * @author ciri-cuervo
 */
@Configuration
public class WebConfiguration extends WebMvcConfigurerAdapter {

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addRedirectViewController("/", "/home");

		registry.addViewController("/home").setViewName(Views.Pages.Misc.HOME_PAGE);
	}

	/*
	 * Add interceptors.
	 * LocaleChangeInterceptor: intercepts requests that contain the param 'locale', and changes the application locale.
	 * RequestCacheClearInterceptor: clears request cache when user navigates the site
	 * AnonymousRestrictedHandlerInterceptor: redirects to home page when user is authenticated and method is @Anonymous
	 */

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new LocaleChangeInterceptor());
		registry.addInterceptor(new NegatedRequestCacheClearInterceptor("/account/**"));
		registry.addInterceptor(new AnonymousRestrictedHandlerInterceptor());
	}

	/*
	 * Define 'localeResolver' bean. Stores the locale and time zone in a cookie.
	 */

	@Bean
	@ConditionalOnProperty(prefix = "spring.mvc", name = "locale")
	public LocaleResolver localeResolver(@Value("${spring.mvc.locale}") Locale locale) {
		CookieLocaleResolver localeResolver = new CookieLocaleResolver();
		localeResolver.setDefaultLocale(locale);
		return localeResolver;
	}

}
