/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.config;

import static org.ciricuervo.rse.domain.type.AuthorityType.*;

import java.util.LinkedHashMap;

import org.ciricuervo.rse.properties.ApplicationProperties;
import org.ciricuervo.rse.repository.EmailHashRepository;
import org.ciricuervo.rse.repository.UserRepository;
import org.ciricuervo.rse.security.CustomAuthenticationFailureHandler;
import org.ciricuervo.rse.security.CustomAuthenticationSuccessHandler;
import org.ciricuervo.rse.security.DelegatingRedirectAccessDeniedHandler;
import org.ciricuervo.rse.security.RedirectAccessDeniedHandler;
import org.ciricuervo.rse.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.access.AccessDeniedHandlerImpl;
import org.springframework.security.web.access.DelegatingAccessDeniedHandler;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.csrf.InvalidCsrfTokenException;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

/**
 * Spring Security configuration.
 * 
 * @author ciri-cuervo
 */
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	private ApplicationProperties applicationProperties;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private EmailHashRepository emailHashRepository;
	@Autowired
	private UserService userService;
	@Autowired
	private UserDetailsService userDetailsService;
	@Autowired
	private SimpMessagingTemplate messagingTemplate;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
				.antMatchers("/", "/home").permitAll()

				.antMatchers("/admin/**").hasAuthority(ROLE_ADMIN.toString())

				.antMatchers(HttpMethod.GET,  "/account/login").permitAll() // redirects if authenticated
				.antMatchers(HttpMethod.POST, "/account/login").anonymous()

				.antMatchers("/account/logout").authenticated()

				.antMatchers(HttpMethod.GET,  "/account/signup").permitAll() // redirects if authenticated
				.antMatchers(HttpMethod.POST, "/account/signup").anonymous()
				.antMatchers(HttpMethod.GET,  "/account/signup/confirm").permitAll() // logs out if authenticated

				.antMatchers(HttpMethod.GET,  "/account/activate").permitAll() // redirects if authenticated
				.antMatchers(HttpMethod.POST, "/account/activate").anonymous()

				.antMatchers(HttpMethod.GET,  "/account/unlock").permitAll() // redirects if authenticated
				.antMatchers(HttpMethod.POST, "/account/unlock").anonymous()
				.antMatchers(HttpMethod.GET,  "/account/unlock/confirm").permitAll() // logs out if authenticated

				.antMatchers(HttpMethod.GET,  "/account/password/reset").permitAll() // redirects if authenticated
				.antMatchers(HttpMethod.POST, "/account/password/reset").anonymous()
				.antMatchers(HttpMethod.GET,  "/account/password/reset/confirm").permitAll() // logs out if authenticated
				.antMatchers(HttpMethod.POST, "/account/password/reset/confirm").anonymous()

				.antMatchers("/account/**").authenticated()
				.antMatchers("/websocket/**").authenticated()
				.anyRequest().permitAll()

			.and().formLogin()
				.loginPage("/account/login")
				.successHandler(successHandler("/home"))
				.failureHandler(failureHandler("/account/login?error"))

			.and().logout()
				.logoutUrl("/account/logout")
				.logoutSuccessUrl("/home")

			.and().exceptionHandling()
				.accessDeniedHandler(accessDeniedHandler("/account/logout", "/"))

			.and().csrf()
			.and().rememberMe();
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring()
				.antMatchers("/webjars/**", "/webjarslocator/**")
				.antMatchers("/robots.xml", "/sitemap.xml");
	}

	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth
			.userDetailsService(userDetailsService)
			.passwordEncoder(new BCryptPasswordEncoder());
	}

	@Bean
	public AuthenticationSuccessHandler successHandler(String defaultSuccessUrl) {
		CustomAuthenticationSuccessHandler successHandler = new CustomAuthenticationSuccessHandler(userRepository, emailHashRepository);
		successHandler.setDefaultTargetUrl(defaultSuccessUrl);
		return successHandler;
	}

	@Bean
	public AuthenticationFailureHandler failureHandler(String defaultFailureUrl) {
		CustomAuthenticationFailureHandler failureHandler = new CustomAuthenticationFailureHandler(applicationProperties, userService);
		failureHandler.setDefaultFailureUrl(defaultFailureUrl);
		return failureHandler;
	}

	@Bean
	public AccessDeniedHandler accessDeniedHandler(String defaultTargetUrl, String defaultRedirectUrl) {
		AccessDeniedHandlerImpl defaultHandler = new AccessDeniedHandlerImpl();

		RedirectAccessDeniedHandler accessDeniedHandler = new RedirectAccessDeniedHandler();
		accessDeniedHandler.setDefaultRedirectUrl(defaultRedirectUrl);

		LinkedHashMap<RequestMatcher, RedirectAccessDeniedHandler> redirectHandlers = new LinkedHashMap<>();
		redirectHandlers.put(new AntPathRequestMatcher(defaultTargetUrl), accessDeniedHandler);
		DelegatingRedirectAccessDeniedHandler redirectHandler =
				new DelegatingRedirectAccessDeniedHandler(redirectHandlers, defaultHandler);

		LinkedHashMap<Class<? extends AccessDeniedException>, AccessDeniedHandler> handlers = new LinkedHashMap<>();
		handlers.put(InvalidCsrfTokenException.class, redirectHandler);
		return new DelegatingAccessDeniedHandler(handlers, defaultHandler);
	}

}
