/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * Helper class to access statically to the application context.
 * 
 * @author ciri-cuervo
 */
@Component
public class ApplicationContextProvider implements ApplicationContextAware {

	public static ApplicationContext ctx;

	@Override
	public void setApplicationContext(final ApplicationContext applicationContext) throws BeansException {
		ctx = applicationContext;
	}

}
