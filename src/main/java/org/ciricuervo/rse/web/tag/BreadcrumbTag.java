/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.web.tag;

import lombok.Data;

/**
 * Breadcrumb tag object representation.
 * 
 * @author ciri-cuervo
 */
@Data
public class BreadcrumbTag {

	private final String text;
	private final String href;

}
