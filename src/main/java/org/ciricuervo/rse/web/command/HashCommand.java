/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.web.command;

import javax.validation.constraints.Pattern;

import org.ciricuervo.rse.util.Regexp;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Hash command.
 * 
 * @author ciri-cuervo
 */
@Getter
@Setter
@ToString
public class HashCommand {

	@Pattern(regexp = Regexp.HASH)
	private String hash;

}
