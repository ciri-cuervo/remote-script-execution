/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.web.command;

import java.util.Optional;

import org.ciricuervo.rse.domain.User;
import org.ciricuervo.rse.domain.type.UserStatusType;
import org.ciricuervo.rse.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Email command validator.
 * 
 * @author ciri-cuervo
 */
@Component
public class EmailCommandValidator implements Validator {

	@Autowired
	private UserRepository userRepository;

	private UserStatusType userStatus;

	public EmailCommandValidator() {}

	public EmailCommandValidator(UserStatusType userStatus) {
		this.userStatus = userStatus;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return EmailCommand.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		EmailCommand command = (EmailCommand) target;

		if (!errors.hasFieldErrors("email")) {
			Optional<User> user = userRepository.findByEmail(command.getEmail());
			if (!user.filter(u -> userStatus == null || userStatus.equals(u.getStatus())).isPresent()) {
				errors.rejectValue("email", "NotExists");
			}
		}
	}

}
