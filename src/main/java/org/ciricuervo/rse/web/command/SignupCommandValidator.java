/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.web.command;

import java.util.Objects;
import java.util.Optional;

import org.ciricuervo.rse.domain.User;
import org.ciricuervo.rse.properties.ApplicationProperties;
import org.ciricuervo.rse.repository.UserRepository;
import org.ciricuervo.rse.util.DomainMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Sign-up command validator.
 * 
 * @author ciri-cuervo
 */
@Component
public class SignupCommandValidator implements Validator {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private ApplicationProperties appProps;

	@Override
	public boolean supports(Class<?> clazz) {
		return SignupCommand.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		SignupCommand command = (SignupCommand) target;

		if (!errors.hasFieldErrors("username")) {
			Optional<User> user = userRepository.findByUsername(command.getUsername());
			user.ifPresent(u -> errors.rejectValue("username", "Exists"));
		}

		if (!errors.hasFieldErrors("email")) {
			Optional<User> user = userRepository.findByEmail(command.getEmail());
			user.ifPresent(u -> errors.rejectValue("email", "Exists"));
		}

		if (!errors.hasFieldErrors("email")) {
			String domain = command.getEmail().substring(command.getEmail().indexOf("@") + 1);
			if (appProps.getAllowedEmailDomains().stream().noneMatch(p -> DomainMatcher.match(p, domain))) {
				errors.rejectValue("email", "NotAuthorized");
			}
		}

		if (!errors.hasFieldErrors("password")) {
			if (!errors.hasFieldErrors("username") && Objects.equals(command.getUsername(), command.getPassword())
					|| !errors.hasFieldErrors("email") && Objects.equals(command.getEmail(), command.getPassword())) {
				errors.rejectValue("password", "Invalid");
			}
		}

		if (!errors.hasFieldErrors("password") && !errors.hasFieldErrors("repeatPassword")
				&& !Objects.equals(command.getPassword(), command.getRepeatPassword())) {
			errors.rejectValue("repeatPassword", "Pattern");
		}
	}

}
