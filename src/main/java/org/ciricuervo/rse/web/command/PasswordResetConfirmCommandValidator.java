/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.web.command;

import java.util.Objects;

import org.ciricuervo.rse.repository.EmailHashRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Password Reset Confirm command validator.
 * 
 * @author ciri-cuervo
 */
@Component
public class PasswordResetConfirmCommandValidator implements Validator {

	@Autowired
	private EmailHashRepository emailHashRepository;

	@Override
	public boolean supports(Class<?> clazz) {
		return PasswordResetConfirmCommand.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		PasswordResetConfirmCommand command = (PasswordResetConfirmCommand) target;

		if (!errors.hasFieldErrors("hash")) {
			if (!emailHashRepository.findByHashAndActiveTrue(command.getHash()).isPresent()) {
				errors.reject("NotActive.passwordResetConfirmCommand");
			}
		}

		if (!errors.hasFieldErrors("password") && !errors.hasFieldErrors("repeatPassword")
				&& !Objects.equals(command.getPassword(), command.getRepeatPassword())) {
			errors.rejectValue("repeatPassword", "Pattern");
		}
	}

}
