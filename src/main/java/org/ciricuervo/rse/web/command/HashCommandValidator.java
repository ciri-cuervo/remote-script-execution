/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.web.command;

import java.util.Optional;

import org.ciricuervo.rse.domain.EmailHash;
import org.ciricuervo.rse.repository.EmailHashRepository;
import org.ciricuervo.rse.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Hash command validator.
 * 
 * @author ciri-cuervo
 */
@Component
public class HashCommandValidator implements Validator {

	@Autowired
	private EmailHashRepository emailHashRepository;
	@Autowired
	private UserRepository userRepository;

	@Override
	public boolean supports(Class<?> clazz) {
		return HashCommand.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		HashCommand command = (HashCommand) target;

		if (!errors.hasFieldErrors("hash")) {
			Optional<EmailHash> emailHash = emailHashRepository.findByHashAndActiveTrue(command.getHash());
			if (!emailHash.isPresent() || !userRepository.findByEmail(emailHash.get().getEmail()).isPresent()) {
				errors.reject("NotActive.hashCommand");
			}
		}
	}

}
