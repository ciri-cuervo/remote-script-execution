/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.web.command;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Pattern;

import org.ciricuervo.rse.util.Regexp;
import org.hibernate.validator.constraints.NotEmpty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Sign-up command.
 * 
 * @author ciri-cuervo
 */
@Getter
@Setter
@ToString
public class SignupCommand {

	@Pattern(regexp = Regexp.USERNAME)
	private String username;

	@Pattern(regexp = Regexp.EMAIL)
	private String email;

	@Pattern(regexp = Regexp.PASSWORD)
	private String password;

	@NotEmpty
	private String repeatPassword;

	@AssertTrue
	private boolean accept;

}
