/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.web.command;

import org.ciricuervo.rse.domain.type.UserStatusType;
import org.springframework.stereotype.Component;

/**
 * Password Reset email command validator.
 * 
 * @author ciri-cuervo
 */
@Component
public class PasswordResetCommandValidator extends EmailCommandValidator {

	public PasswordResetCommandValidator() {
		super(UserStatusType.ACTIVE);
	}

}
