/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.web.command;

import javax.validation.constraints.Pattern;

import org.ciricuervo.rse.util.Regexp;
import org.hibernate.validator.constraints.NotEmpty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Password Reset command.
 * 
 * @author ciri-cuervo
 */
@Getter
@Setter
@ToString
public class PasswordResetConfirmCommand {

	@Pattern(regexp = Regexp.HASH)
	private String hash;

	@Pattern(regexp = Regexp.PASSWORD)
	private String password;

	@NotEmpty
	private String repeatPassword;

}
