/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.web.controller;

import org.ciricuervo.rse.domain.Execution;
import org.ciricuervo.rse.listener.ExecutionStartedEvent;
import org.ciricuervo.rse.service.RunningExecutionService;
import org.ciricuervo.rse.service.remoteexecution.RemoteExecutionException;
import org.ciricuervo.rse.service.remoteexecution.ScriptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller that provides services to run a script.
 * 
 * @author ciri-cuervo
 */
@RestController
@RequestMapping("/scripts")
public class ScriptController {

	@Autowired
	private ScriptService scriptService;
	@Autowired
	private RunningExecutionService runningExecutionService;
	@Autowired
	private ApplicationEventPublisher eventPublisher;

	@RequestMapping(value = "/{id}/run", method = RequestMethod.GET)
	public Execution runOne(@PathVariable Long id) throws RemoteExecutionException {
		Execution execution = scriptService.runScript(id);
		// add the current execution to the running executions group
		if (runningExecutionService.add(execution)) {
			eventPublisher.publishEvent(new ExecutionStartedEvent(this, execution));
		}
		return execution;
	}

}
