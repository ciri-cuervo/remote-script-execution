/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.web.controller;

import java.util.List;
import java.util.Map;

import org.ciricuervo.rse.domain.Server;
import org.ciricuervo.rse.service.remoteexecution.MonitorService;
import org.ciricuervo.rse.service.remoteexecution.RemoteExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller that provides services to monitor a server.
 * 
 * @author ciri-cuervo
 */
@RestController
@RequestMapping("/servers")
public class ServerController {

	@Autowired
	private MonitorService monitorService;

	@RequestMapping(value = "/monitor", method = RequestMethod.GET)
	public Map<Server, List<String>> monitorAll() throws RemoteExecutionException {
		return monitorService.monitorServers();
	}

	@RequestMapping(value = "/{credentialId}/monitor", method = RequestMethod.GET)
	public String monitorOne(@PathVariable Long credentialId) throws RemoteExecutionException {
		return monitorService.monitorServer(credentialId);
	}

}
