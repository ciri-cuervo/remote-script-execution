/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.web.controller;

import org.ciricuervo.rse.repository.ScriptRepository;
import org.ciricuervo.rse.repository.ServerRepository;
import org.ciricuervo.rse.util.WebConstants.Views.Pages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Admin page controller.
 * 
 * @author ciri-cuervo
 */
@Controller
@RequestMapping("/admin")
public class AdminPageController {

	@Autowired
	private ServerRepository serverRepository;
	@Autowired
	private ScriptRepository scriptRepository;

	@RequestMapping
	public String homePage(Model model) {
		return Pages.Admin.HOME_PAGE;
	}

	@RequestMapping("/scripts")
	public String scriptsPage(Model model) {
		model.addAttribute("scripts", scriptRepository.findAll());
		return Pages.Admin.SCRIPTS_PAGE;
	}

	@RequestMapping("/servers")
	public String serversPage(Model model) {
		model.addAttribute("servers", serverRepository.findAll());
		return Pages.Admin.SERVERS_PAGE;
	}

}
