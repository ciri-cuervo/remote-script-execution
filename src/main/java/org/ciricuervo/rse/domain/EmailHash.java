/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.domain;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

import org.ciricuervo.rse.domain.type.EmailHashType;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * {@link EmailHash} entity.
 * 
 * @author ciri-cuervo
 */
@Entity
@Table(indexes = {@Index(columnList = "email"), @Index(columnList = "expire")})
@NoArgsConstructor
@Getter
@Setter
@ToString(callSuper = true)
public class EmailHash extends AbstractEntity<Long> {

	@Column(nullable = false, updatable = false)
	private String email;

	@Column(nullable = false, updatable = false, unique = true)
	private String hash;

	@Column(nullable = false, updatable = false, length = 32)
	private EmailHashType type;

	@Column(nullable = false)
	private Boolean active;

	@Column(nullable = false, updatable = false)
	private LocalDateTime expire;

}
