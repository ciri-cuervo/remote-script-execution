/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * {@link Script} entity.
 * 
 * @author ciri-cuervo
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@ToString(callSuper = true, exclude = "description")
public class Script extends AbstractEntity<Long> {

	@Column(nullable = false, unique = true)
	protected String name;

	protected String description;

	@Column(nullable = false)
	protected String path;

	protected String parameters;

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	protected Credential credential;

}
