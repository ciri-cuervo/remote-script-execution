/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.domain.type;

/**
 * {@link org.ciricuervo.rse.domain.User User} authority type.
 * 
 * @author ciri-cuervo
 */
public enum AuthorityType {

	ROLE_ADMIN, ROLE_USER, ROLE_SYSTEM;

}
