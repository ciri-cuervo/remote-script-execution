/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.domain.type;

/**
 * {@link org.ciricuervo.rse.domain.EmailHash EmailHash} type.
 * 
 * @author ciri-cuervo
 */
public enum EmailHashType {

	/**
	 * Account activation {@link org.ciricuervo.rse.domain.EmailHash EmailHash}.
	 */
	ACCOUNT_ACT,
	/**
	 * Account unlock {@link org.ciricuervo.rse.domain.EmailHash EmailHash}.
	 */
	ACCOUNT_UNLOCK,
	/**
	 * Password restore {@link org.ciricuervo.rse.domain.EmailHash EmailHash}.
	 */
	PWD_RESTORE;

}
