/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.domain.type;

/**
 * {@link org.ciricuervo.rse.domain.User User} status type.
 * 
 * @author ciri-cuervo
 */
public enum UserStatusType {

	/**
	 * Active account {@link org.ciricuervo.rse.domain.User User}.
	 */
	ACTIVE,
	/**
	 * Not activated account {@link org.ciricuervo.rse.domain.User User}.
	 */
	WAITING_ACTIVATION,
	/**
	 * Locked account {@link org.ciricuervo.rse.domain.User User}.
	 */
	LOCKED;

}
