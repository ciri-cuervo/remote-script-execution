/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * {@link Credential} entity.
 * 
 * @author ciri-cuervo
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@ToString(callSuper = true, exclude = "pass")
public class Credential extends AbstractEntity<Long> {

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	protected Server server;

	@Column(nullable = false)
	protected String user;

	@JsonIgnore
	@Column(nullable = false)
	protected String pass;

}
