/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.domain;

import static java.util.stream.Collectors.*;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;

import org.ciricuervo.rse.domain.type.AuthorityType;
import org.ciricuervo.rse.domain.type.UserStatusType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.Assert;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * {@link User} entity.
 * 
 * @author ciri-cuervo
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@ToString(callSuper = true, exclude = "password")
public class User extends AbstractEntity<Long> implements UserDetails {

	private static final long serialVersionUID = 2601721411763278400L;

	@Column(nullable = false, unique = true)
	protected String username;

	@JsonIgnore
	@Column(nullable = false)
	protected String password;

	@Column(nullable = false, unique = true)
	protected String email;

	protected String firtname;

	protected String lastname;

	protected String address;

	protected String phone;

	protected String info;

	protected LocalDate dob;

	@Column(nullable = false)
	protected Integer loginAttempts = 0;

	@Column(nullable = false, length = 32)
	@Enumerated(EnumType.STRING)
	protected UserStatusType status;

	@Column(nullable = false, length = 32)
	@Enumerated(EnumType.STRING)
	@ElementCollection(fetch = FetchType.EAGER)
	protected final Set<AuthorityType> authorities = new HashSet<>(0);

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return !UserStatusType.LOCKED.equals(status);
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return UserStatusType.ACTIVE.equals(status);
	}

	@Override
	public List<? extends GrantedAuthority> getAuthorities() {
		return authorities.stream().map(a -> new SimpleGrantedAuthority(a.toString())).collect(toList());
	}

	public boolean addAuthority(AuthorityType authority) {
		Assert.notNull(authority);
		return authorities.add(authority);
	}

	public boolean removeAuthority(AuthorityType authority) {
		return authorities.remove(authority);
	}

	public boolean hasAuthority(AuthorityType authority) {
		return authorities.contains(authority);
	}

}
