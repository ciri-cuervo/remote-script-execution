/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.domain;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;

import org.springframework.util.Assert;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * {@link Execution} entity.
 * 
 * @author ciri-cuervo
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@ToString(callSuper = true, exclude = "script")
public class Execution extends AbstractEntity<Long> {

	@JsonIgnore
	@Transient
	protected Script script;

	@Column(nullable = false, updatable = false)
	protected String username;

	@Column(updatable = false)
	protected String pid;

	@Column(nullable = false, updatable = false)
	protected LocalDateTime timeStart;

	@Column(updatable = false)
	protected LocalDateTime timeEnd;

	@Column(updatable = false)
	protected String scriptName;

	@Column(updatable = false)
	protected String scriptPath;

	@Column(updatable = false)
	protected String scriptParameters;

	@Column(updatable = false)
	protected String serverIp;

	@Column(updatable = false)
	protected String serverHost;

	@Column(updatable = false)
	protected String serverName;

	@Column(updatable = false)
	protected String serverProtocol;

	@Column(updatable = false)
	protected Integer serverPort;

	@Column(updatable = false)
	protected String credentialUser;

	/**
	 * {@link Execution} constructor method.
	 * 
	 * @param script the script being executed
	 */
	public Execution(Script script) {
		Assert.notNull(script, "The script can't be empty.");
		this.script = script;
		this.scriptName = script.getName();
		this.scriptPath = script.getPath();
		this.scriptParameters = script.getParameters();
		this.serverIp = script.getCredential().getServer().getIp();
		this.serverHost = script.getCredential().getServer().getHost();
		this.serverName = script.getCredential().getServer().getName();
		this.serverProtocol = script.getCredential().getServer().getProtocol();
		this.serverPort = script.getCredential().getServer().getPort();
		this.credentialUser = script.getCredential().getUser();
	}

}
