/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.util.UrlUtils;
import org.springframework.util.Assert;

/**
 * .
 * 
 * @author ciri-cuervo
 */
public class RedirectAccessDeniedHandler implements AccessDeniedHandler {

	protected String defaultRedirectUrl = "/";

	@Override
	public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException)
			throws IOException, ServletException {
		if (!response.isCommitted()) {
			response.sendRedirect(defaultRedirectUrl);
		}
	}

	/**
	 * The redirect page to use. Must begin with a "/" and is interpreted relative to the current context root.
	 */
	public void setDefaultRedirectUrl(String defaultRedirectUrl) {
		Assert.isTrue(UrlUtils.isValidRedirectUrl(defaultRedirectUrl), "'" + defaultRedirectUrl + "' is not a valid redirect URL");
		this.defaultRedirectUrl = defaultRedirectUrl;
	}

}
