/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ciricuervo.rse.domain.User;
import org.ciricuervo.rse.repository.EmailHashRepository;
import org.ciricuervo.rse.repository.UserRepository;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

/**
 * When user authentication succeeds:<br>
 * 
 * <ul>
 * <li>Clear login attempts.</li>
 * <li>Deactivate all {@link org.ciricuervo.rse.domain.EmailHash EmailHash} for user.</li>
 * <li>Redirect to saved request.</li>
 * </ul>
 * 
 * @author ciri-cuervo
 */
public class CustomAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

	protected final UserRepository userRepository;
	protected final EmailHashRepository emailHashRepository;

	/**
	 * .
	 */
	public CustomAuthenticationSuccessHandler(UserRepository userRepository, EmailHashRepository emailHashRepository) {
		this.userRepository = userRepository;
		this.emailHashRepository = emailHashRepository;
	}

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
			throws ServletException, IOException {

		// reset login attempts for user
		User user = (User) authentication.getPrincipal();
		user.setLoginAttempts(0);
		user = userRepository.save(user);

		// deactivate all email-hash for user
		emailHashRepository.deactivateByEmail(user.getEmail());

		super.onAuthenticationSuccess(request, response, authentication);
	}

}
