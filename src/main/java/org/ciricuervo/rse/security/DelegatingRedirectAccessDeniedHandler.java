package org.ciricuervo.rse.security;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.util.Assert;

/**
 * .
 * 
 * @author ciri-cuervo
 */
public final class DelegatingRedirectAccessDeniedHandler implements AccessDeniedHandler {

	private final LinkedHashMap<RequestMatcher, RedirectAccessDeniedHandler> handlers;

	private final AccessDeniedHandler defaultHandler;

	/**
	 * Creates a new instance
	 *
	 * @param handlers a map of the {@link AccessDeniedException} class to the {@link AccessDeniedHandler} that should be used. Each is
	 *        considered in the order they are specified and only the first {@link AccessDeniedHandler} is used.
	 * @param defaultHandler the default {@link AccessDeniedHandler} that should be used if none of the handlers matches.
	 */
	public DelegatingRedirectAccessDeniedHandler(LinkedHashMap<RequestMatcher, RedirectAccessDeniedHandler> handlers,
			AccessDeniedHandler defaultHandler) {
		Assert.notEmpty(handlers, "handlers cannot be null or empty");
		Assert.notNull(defaultHandler, "defaultHandler cannot be null");
		this.handlers = handlers;
		this.defaultHandler = defaultHandler;
	}

	@Override
	public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException)
			throws IOException, ServletException {
		for (Entry<RequestMatcher, RedirectAccessDeniedHandler> entry : handlers.entrySet()) {
			RequestMatcher requestMatcher = entry.getKey();
			if (requestMatcher.matches(request)) {
				AccessDeniedHandler handler = entry.getValue();
				handler.handle(request, response, accessDeniedException);
				return;
			}
		}
		defaultHandler.handle(request, response, accessDeniedException);
	}

}
