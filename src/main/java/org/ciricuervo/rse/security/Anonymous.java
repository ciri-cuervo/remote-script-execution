/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.security;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.core.annotation.AliasFor;

/**
 * .
 * 
 * @author ciri-cuervo
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Anonymous {

	/**
	 * @return the redirect URL
	 */
	@AliasFor("redirect")
	String value() default "/";

	/**
	 * @return the redirect URL
	 */
	@AliasFor("value")
	String redirect() default "/";

	/**
	 * @return whether to append the current query to the redirect URL
	 */
	boolean appendQuery() default false;

	/**
	 * @return whether to logout the authenticated principal before redirection
	 */
	boolean logout() default false;

}
