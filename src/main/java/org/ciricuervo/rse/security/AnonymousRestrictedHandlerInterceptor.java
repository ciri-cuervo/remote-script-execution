/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ciricuervo.rse.util.HttpUtils;
import org.ciricuervo.rse.util.SecurityUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import lombok.extern.slf4j.Slf4j;

/**
 * .
 * 
 * @author ciri-cuervo
 */
@Slf4j
public class AnonymousRestrictedHandlerInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		// only HandlerMethod can be annotated with @Anonymous annotation
		if (!(handler instanceof HandlerMethod)) {
			return true;
		}
		// already using anonymous authentication
		if (SecurityUtils.isAnonymous()) {
			return true;
		}

		HandlerMethod handlerMethod = (HandlerMethod) handler;

		log.trace("Looking for Anonymous annotation for method '{}' on class '{}'", handlerMethod.getMethod().getName(), handlerMethod
				.getBeanType().getName());

		Anonymous anonymous = handlerMethod.getMethodAnnotation(Anonymous.class);

		// annotation not present
		if (anonymous == null) {
			return true;
		}

		if (anonymous.logout()) {
			log.debug("Logout current authenticated principal");
			request.logout();
		}

		String redirectUrl = anonymous.redirect();

		if (anonymous.appendQuery()) {
			redirectUrl = HttpUtils.appendCurrentQueryParams(redirectUrl, request);
		}

		log.trace("Redirect to '{}'", redirectUrl);
		response.sendRedirect(redirectUrl);
		return false;
	}

}
