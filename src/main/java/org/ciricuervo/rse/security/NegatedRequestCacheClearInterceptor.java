/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.security;

import org.springframework.http.HttpMethod;
import org.springframework.security.web.util.matcher.AndRequestMatcher;
import org.springframework.security.web.util.matcher.NegatedRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

/**
 * This {@link org.springframework.web.servlet.HandlerInterceptor HandlerInterceptor} clears the request cache generated by a
 * {@link org.springframework.security.access.AccessDeniedException AccessDeniedException}, after the user declines a login action. Used to
 * avoid the redirection to an old requested secure page when the user logs in.
 * 
 * @author ciri-cuervo
 */
public class NegatedRequestCacheClearInterceptor extends RequestCacheClearInterceptor {

	/**
	 * {@link NegatedRequestCacheClearInterceptor} constructor.
	 */
	public NegatedRequestCacheClearInterceptor(RequestMatcher requestMatcher) {
		super(new NegatedRequestMatcher(requestMatcher));
	}

	/**
	 * {@link NegatedRequestCacheClearInterceptor} constructor.
	 */
	public NegatedRequestCacheClearInterceptor(String... patterns) {
		this(null, patterns);
	}

	/**
	 * {@link NegatedRequestCacheClearInterceptor} constructor.
	 */
	public NegatedRequestCacheClearInterceptor(HttpMethod httpMethod, String... patterns) {
		super(AndRequestMatcher::new, createRequestMatchers(NegatedRequestMatcher::new, httpMethod, patterns));
	}

}
