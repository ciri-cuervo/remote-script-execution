/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ciricuervo.rse.properties.ApplicationProperties;
import org.ciricuervo.rse.service.UserService;
import org.ciricuervo.rse.util.HttpUtils;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

/**
 * When user authentication fails:<br>
 * 
 * <ul>
 * <li>Add login attempt. Lock user account if reaches the max allowed.</li>
 * <li>Redirect to login page with error information.</li>
 * </ul>
 * 
 * @author ciri-cuervo
 */
public class CustomAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

	protected final ApplicationProperties appProps;
	protected final UserService userService;
	protected String defaultFailureUrl;

	/**
	 * .
	 */
	public CustomAuthenticationFailureHandler(ApplicationProperties appProps, UserService userService) {
		this.appProps = appProps;
		this.userService = userService;
	}

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception)
			throws IOException, ServletException {

		String username = request.getParameter("username");
		int remainingAttempts = userService.sumLoginAttempt(username);

		String failureUrl = defaultFailureUrl;
		if (remainingAttempts > -1 && remainingAttempts <= appProps.getLoginAttemptsThreshold()) {
			failureUrl = HttpUtils.appendParameterToQuery(failureUrl, "r", remainingAttempts, "UTF-8");
		}

		saveException(request, exception);
		getRedirectStrategy().sendRedirect(request, response, failureUrl);
	}

	@Override
	public void setDefaultFailureUrl(String defaultFailureUrl) {
		super.setDefaultFailureUrl(defaultFailureUrl);
		this.defaultFailureUrl = defaultFailureUrl;
	}

}
