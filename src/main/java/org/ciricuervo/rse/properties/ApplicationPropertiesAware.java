/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.properties;

import org.springframework.beans.factory.Aware;

/**
 * .
 * 
 * @author ciri-cuervo
 */
public interface ApplicationPropertiesAware extends Aware {

	/**
	 * .
	 */
	void setApplicationProperties(ApplicationProperties applicationProperties);

}
