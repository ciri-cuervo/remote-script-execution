/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.properties;

import org.springframework.stereotype.Component;

/**
 * Helper service to access statically to the application properties.
 * 
 * @author ciri-cuervo
 */
@Component
public class ApplicationPropertiesProvider implements ApplicationPropertiesAware {

	public static ApplicationProperties props;

	@Override
	public void setApplicationProperties(final ApplicationProperties applicationProperties) {
		props = applicationProperties;
	}

}
