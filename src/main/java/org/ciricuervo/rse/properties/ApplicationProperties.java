/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.properties;

import java.util.Collections;
import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

/**
 * Configuration properties for the application.
 *
 * @author ciri-cuervo
 */
@Component
@ConfigurationProperties(prefix = "application.rse")
@Getter
@Setter
public class ApplicationProperties {

	/* Default values */

	private static final Integer DEFAULT_TASK_PARALLELISM = 5;
	private static final Integer DEFAULT_HASH_LENGTH = 24;
	private static final Integer DEFAULT_HASH_EXPIRE_HS = 24;
	private static final Integer DEFAULT_MAX_LOGIN_ATTEMPTS = 12;
	private static final Integer DEFAULT_LOGIN_ATTEMPTS_THRESHOLD = 3;

	/* Members */

	private Integer taskParallelism = DEFAULT_TASK_PARALLELISM;
	private Integer hashLength = DEFAULT_HASH_LENGTH;
	private Integer hashExpireHs = DEFAULT_HASH_EXPIRE_HS;
	private Integer maxLoginAttemps = DEFAULT_MAX_LOGIN_ATTEMPTS;
	private Integer loginAttemptsThreshold = DEFAULT_LOGIN_ATTEMPTS_THRESHOLD;
	private List<String> allowedEmailDomains = Collections.singletonList("*");

	private String temporalDir;
	private String storageDir;

}
