/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.service;

import java.io.IOException;
import java.util.Optional;

import org.ciricuervo.rse.domain.User;
import org.ciricuervo.rse.domain.type.UserStatusType;
import org.ciricuervo.rse.properties.ApplicationProperties;
import org.ciricuervo.rse.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

/**
 * Provides functionality to manage {@link User}.
 * 
 * @author ciri-cuervo
 */
@Service
@Transactional
@Slf4j
public class UserService {

	@Autowired
	private ApplicationProperties appProps;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private PathManagerService pathManagerService;

	/**
	 * Gets a {@link User} by {@code username} or {@code email}.
	 */
	@Transactional(readOnly = true)
	public Optional<User> findByUsernameOrEmail(String username) {
		return username.contains("@") ? userRepository.findByEmail(username) : userRepository.findByUsername(username);
	}

	/**
	 * Delete a {@link User} and its dependencies.
	 */
	public void deleteUser(User user) {
		if (user != null) {
			log.info("Delete user: {}", user);
			userRepository.delete(user);
			try {
				pathManagerService.deleteUserStoragePath(user);
			} catch (IOException e) {
				log.error("Coulnd't delete storage directory for user [id=" + user.getId() + ", username=" + user.getUsername() + "]", e);
			}
		}
	}

	/**
	 * Adds a login attempt to a {@link User}. If user reaches max login attempts its account will be locked.
	 */
	public int sumLoginAttempt(String username) {
		User user = this.findByUsernameOrEmail(username).orElse(null);

		if (user == null) {
			return -1;
		}

		int attempts = user.getLoginAttempts() + 1;
		int remaining = Math.max(appProps.getMaxLoginAttemps() - attempts, 0);
		user.setLoginAttempts(attempts);

		log.debug("User login fail: {}", user);

		if (remaining == 0) {
			user.setStatus(UserStatusType.LOCKED);
			log.info("User locked, reached max login attempts: {}", user);
		}

		user = userRepository.save(user);
		return remaining;
	}

}
