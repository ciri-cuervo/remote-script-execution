/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.service;

import static java.util.stream.Collectors.*;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.ciricuervo.rse.domain.Credential;
import org.ciricuervo.rse.domain.Execution;
import org.ciricuervo.rse.util.MultiValueMapUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

/**
 * Provides functionality to manage running {@link Execution}.
 * 
 * @author ciri-cuervo
 */
@Service
public class RunningExecutionService {

	private Map<Credential, Set<Execution>> runningExecutions = new HashMap<>();

	/**
	 * Adds a running {@link Execution}.
	 */
	public boolean add(Execution execution) {
		Assert.notNull(execution, "The execution can't be null.");
		Assert.hasText(execution.getPid(), "The execution must have a valid PID.");
		synchronized (runningExecutions) {
			return MultiValueMapUtils.add(runningExecutions, execution.getScript().getCredential(), execution, HashSet::new);
		}
	}

	/**
	 * Removes a running {@link Execution}.
	 */
	public boolean remove(Execution execution) {
		Assert.notNull(execution, "The execution can't be null.");
		synchronized (runningExecutions) {
			return MultiValueMapUtils.remove(runningExecutions, execution.getScript().getCredential(), execution);
		}
	}

	/**
	 * Returns {@code true} if the given {@link Execution} is running.
	 */
	public boolean contains(Execution execution) {
		Assert.notNull(execution, "The execution can't be null.");
		synchronized (runningExecutions) {
			return MultiValueMapUtils.contains(runningExecutions, execution.getScript().getCredential(), execution);
		}
	}

	/**
	 * Returns the running {@link Execution} grouped by {@link Credential}.
	 */
	public Map<Credential, Set<Execution>> getAll() {
		synchronized (runningExecutions) {
			// creates a copy of the multi-value map structure
			return runningExecutions.entrySet().stream().collect(toMap(Entry::getKey, e -> new HashSet<>(e.getValue())));
		}
	}

	/**
	 * Returns {@code true} if there is at least one running execution.
	 */
	public boolean hasRunningExecutions() {
		synchronized (runningExecutions) {
			return !runningExecutions.isEmpty();
		}
	}

	/**
	 * The amount of running executions.
	 */
	public int countRunningExecutions() {
		synchronized (runningExecutions) {
			return runningExecutions.size();
		}
	}

}
