/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.service.scheduled;

import java.time.LocalDateTime;

import org.ciricuervo.rse.properties.ApplicationProperties;
import org.ciricuervo.rse.repository.EmailHashRepository;
import org.ciricuervo.rse.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * Scheduled tasks that perform general maintenance.
 * 
 * @author ciri-cuervo
 */
@Component
@Profile({"dev", "production"})
@Slf4j
public class MaintenanceTask {

	private static final long ONE_MINUTE = 60 * 1000;
	private static final long ONE_HOUR = 60 * ONE_MINUTE;
	private static final long ONE_DAY = 24 * ONE_HOUR;

	@Autowired
	private ApplicationProperties appProps;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private EmailHashRepository emailHashRepository;

	/**
	 * This job deactivates expired email hashes.
	 */
	@Scheduled(initialDelay = ONE_MINUTE, fixedRate = ONE_HOUR)
	public void deactivateExpiredEmailHash() {
		Integer emailHashDeactivated = emailHashRepository.deactivateByExpired();
		if (emailHashDeactivated > 0) {
			log.info("{} EmailHash were/was deactivated", emailHashDeactivated);
		}
	}

	/**
	 * This job deletes deactivated email hashes.
	 */
	@Scheduled(initialDelay = ONE_HOUR, fixedRate = ONE_DAY)
	public void deleteInactiveEmailHash() {
		Integer emailHashDeleted = emailHashRepository.deleteByActiveFalse();
		if (emailHashDeleted > 0) {
			log.info("{} EmailHash were/was deleted", emailHashDeleted);
		}
	}

	/**
	 * This job deletes users waiting for activation.
	 */
	@Scheduled(initialDelay = ONE_HOUR, fixedRate = ONE_DAY)
	public void deleteWaitingActivationUser() {
		LocalDateTime maxCreationTime = LocalDateTime.now().minusHours(appProps.getHashExpireHs());
		Integer userDeleted = userRepository.deleteByWaitingActivation(maxCreationTime);
		if (userDeleted > 0) {
			log.info("{} User were/was deleted", userDeleted);
		}
	}

}
