/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.service.scheduled;

import java.util.Set;

import org.ciricuervo.rse.domain.Credential;
import org.ciricuervo.rse.listener.ExecutionFinishedEvent;
import org.ciricuervo.rse.service.RunningExecutionService;
import org.ciricuervo.rse.service.remoteexecution.MonitorService;
import org.ciricuervo.rse.service.remoteexecution.RemoteExecutionException;
import org.ciricuervo.rse.util.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * Scheduled task that monitors the servers which have running executions.
 * 
 * @author ciri-cuervo
 */
@Component
@Slf4j
public class ExecutionsMonitorTask {

	@Autowired
	private MonitorService monitorService;
	@Autowired
	private RunningExecutionService runningExecutionService;
	@Autowired
	private ApplicationEventPublisher eventPublisher;

	/**
	 * Updates completed executions.<br>
	 * Fires executionFinished event.
	 */
	@Scheduled(initialDelay = 3000, fixedRate = 5000)
	public void updateCompletedExecutions() {
		if (!runningExecutionService.hasRunningExecutions()) {
			return;
		}

		SecurityUtils.loginSystemUser();

		runningExecutionService.getAll().entrySet().forEach(entry -> {
			Credential credential = entry.getKey();
			log.debug("Executions for {} - {}", credential, entry.getValue().size());

			try {
				Set<String> runningProcesses = monitorService.getRunningProcesses(credential);
				log.debug("Running PIDs {} - {}", credential, runningProcesses);

				entry.getValue().stream()
					.filter(execution -> !runningProcesses.contains(execution.getPid()))
					.filter(execution -> runningExecutionService.remove(execution))
					.forEach(execution -> eventPublisher.publishEvent(new ExecutionFinishedEvent(this, execution)));

			} catch (RemoteExecutionException e) {
				log.error("Error while updating executions. {}", credential, e);
			}
		});

		SecurityUtils.logoutSystemUser();
	}

}
