/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.service;

import java.time.LocalDateTime;
import java.util.UUID;

import org.ciricuervo.rse.domain.EmailHash;
import org.ciricuervo.rse.domain.type.EmailHashType;
import org.ciricuervo.rse.properties.ApplicationProperties;
import org.ciricuervo.rse.repository.EmailHashRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Provides functionality to manage {@link EmailHash}.
 * 
 * @author ciri-cuervo
 */
@Service
public class EmailHashService {

	@Autowired
	private ApplicationProperties appProps;
	@Autowired
	private EmailHashRepository emailHashRepository;

	/**
	 * Create email hash.
	 */
	@Transactional
	public EmailHash createEmailHash(String email, EmailHashType emailType) {
		EmailHash emailHash = new EmailHash();
		emailHash.setEmail(email);
		emailHash.setHash(createHash());
		emailHash.setType(emailType);
		emailHash.setActive(true);
		emailHash.setExpire(LocalDateTime.now().plusHours(appProps.getHashExpireHs()));
		emailHashRepository.deactivateByEmail(emailHash.getEmail());
		return emailHashRepository.save(emailHash);
	}

	public String createHash() {
		return UUID.randomUUID().toString().replace("-", "").substring(0, appProps.getHashLength());
	}

}
