/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.service;

import java.util.Optional;

import org.ciricuervo.rse.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Allows users to login by {@code username} or {@code email}.
 * 
 * @author ciri-cuervo
 */
@Service
@Transactional(readOnly = true)
public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	private UserService userService;

	@Override
	public User loadUserByUsername(final String username) throws UsernameNotFoundException {
		Optional<User> user = userService.findByUsernameOrEmail(username);
		return user.orElseThrow(() -> new UsernameNotFoundException(username + " not found"));
	}

}
