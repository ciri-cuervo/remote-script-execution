/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.service;

import javax.servlet.http.HttpServletRequest;

import org.ciricuervo.rse.util.HttpUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import lombok.Getter;

/**
 * Provides access to the current HTTP Request to services that don't have a reference to it.
 * 
 * @author ciri-cuervo
 */
@Service
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class RequestHelperService {

	@Autowired
	@Getter
	private HttpServletRequest request;

	/**
	 * Returns the base URL of the application: {@code http[s]://domain.com[:8080][/contextpath]}
	 */
	public String getBaseUrl() {
		return HttpUtils.getBaseUrl(request);
	}

	/**
	 * Obtain the request URI without the context path and with the query string appended.
	 */
	public String getRequestPathWithQuery() {
		return HttpUtils.getRequestPathWithQuery(request);
	}

}
