/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.service.remoteexecution;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

import org.ciricuervo.rse.domain.Credential;
import org.ciricuervo.rse.domain.Server;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.client.ResourceAccessException;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.HostKey;
import com.jcraft.jsch.HostKeyRepository;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import lombok.extern.slf4j.Slf4j;

/**
 * Provides functionality to execute commands remotely.
 * 
 * @author ciri-cuervo
 */
@Slf4j
public abstract class RemoteExecutionService {

	protected static final String SSH_KNOWN_HOSTS = "~/.ssh/known_hosts";

	protected static final int SESSION_CONNECT_TIMEOUT = 10000;
	protected static final int CHANNEL_CONNECT_TIMEOUT = 5000;
	protected static final int CHANNEL_EOF_TIMEOUT = 5000;
	protected static final int CHANNEL_EOF_WAIT = 500;
	protected static final int CHANNEL_EOF_WAIT_TRIES = (int) Math.ceil((double) CHANNEL_EOF_TIMEOUT / CHANNEL_EOF_WAIT);

	private static final int OUTPUT_INITIAL_CAPACITY = 1024 * 16;

	/**
	 * Returns the standard output for the given command.
	 */
	protected static String executeCommand(Credential credential, String command) throws RemoteExecutionException {
		log.info("Running: {} - {}", credential, command);

		// initialize standard and error output
		ByteArrayOutputStream os = new ByteArrayOutputStream(OUTPUT_INITIAL_CAPACITY);
		ByteArrayOutputStream es = new ByteArrayOutputStream(OUTPUT_INITIAL_CAPACITY);

		Session session = null;

		try {
			JSch jsch = new JSch();
			jsch.setKnownHosts(SSH_KNOWN_HOSTS);

			addToKnownHostsIfMissing(jsch, credential);

			Server server = credential.getServer();
			session = jsch.getSession(credential.getUser(), server.getHost(), server.getPort());
			session.setPassword(credential.getPass());
			session.connect(SESSION_CONNECT_TIMEOUT);

			ChannelExec channel = (ChannelExec) session.openChannel("exec");
			try {
				// set command and output/error streams and connect
				channel.setCommand(command);
				channel.setOutputStream(new BufferedOutputStream(os));
				channel.setExtOutputStream(new BufferedOutputStream(es));
				channel.connect(CHANNEL_CONNECT_TIMEOUT);
				waitForEof(channel);
			} finally {
				channel.disconnect();
			}
		} catch (Exception e) {
			String message = String.format("Error executing command remotely [credential=%s, command=%s]", credential, command);
			throw new RemoteExecutionException(message, e);
		} finally {
			disconnectQuietly(session);
		}

		try {
			String stdOutput = os.toString(StandardCharsets.UTF_8.name());
			if (!stdOutput.isEmpty()) {
				log.debug("Standard output:\n{}", stdOutput);
			}

			String stdError = es.toString(StandardCharsets.UTF_8.name());
			if (!stdError.isEmpty()) {
				log.debug("Standard error:\n{}", stdError);
			}

			if (stdOutput.isEmpty()) {
				String message = String.format("The remote execution produced an empty output - %s", credential);
				throw new RemoteExecutionException(message, stdError);
			} else {
				return stdOutput;
			}
		} catch (UnsupportedEncodingException e) {
			String message = String.format("Error reading output stream - %s", credential);
			throw new RemoteExecutionException(message, e);
		}
	}

	protected static void addToKnownHostsIfMissing(JSch jsch, Credential credential) throws JSchException {
		Server server = credential.getServer();
		String host = server.getHost() != null ? server.getHost() : server.getIp();
		HostKeyRepository hostKeyRepository = jsch.getHostKeyRepository();
		HostKey[] hostKeys = hostKeyRepository.getHostKey(host, "ssh-rsa");

		if (hostKeys.length == 0) {
			Session session = jsch.getSession(credential.getUser(), server.getHost(), server.getPort());
			try {
				session.connect(SESSION_CONNECT_TIMEOUT);
			} catch (JSchException e) {
				if (session.getHostKey() != null) {
					hostKeyRepository.add(session.getHostKey(), null);
				} else {
					throw e;
				}
			} finally {
				session.disconnect();
			}
		}
	}

	protected static void waitForEof(Channel channel) {
		for (int tries = CHANNEL_EOF_WAIT_TRIES; tries > 0 && !channel.isEOF(); tries--) {
			try {
				Thread.sleep(CHANNEL_EOF_WAIT);
			} catch (InterruptedException e) {
				log.error("Error when waiting for EOF.", e);
			}
		}
	}

	protected static void disconnectQuietly(Session session) {
		if (session != null) {
			session.disconnect();
		}
	}

	/**
	 * Verifies the resource for the given id exists, and returns it.
	 * 
	 * @param repository the repository to search the resource
	 * @param id the resource id
	 * @return the resource for the given id
	 */
	protected static <T, K extends Serializable> T findOne(CrudRepository<T, K> repository, K id) {
		T element = repository.findOne(id);
		if (element == null) {
			throw new ResourceAccessException(String.format("Resource doesn't exist [id = %s]", id));
		}
		return element;
	}

}
