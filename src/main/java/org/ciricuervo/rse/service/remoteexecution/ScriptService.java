/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.service.remoteexecution;

import java.time.LocalDateTime;

import org.ciricuervo.rse.domain.Execution;
import org.ciricuervo.rse.domain.Script;
import org.ciricuervo.rse.repository.ExecutionRepository;
import org.ciricuervo.rse.repository.ScriptRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

/**
 * Provides functionality to manage {@link Script}.
 * 
 * @author ciri-cuervo
 */
@Service
@Transactional(readOnly = true)
@Slf4j
public class ScriptService extends RemoteExecutionService {

	@Autowired
	private ScriptRepository scriptRepository;
	@Autowired
	private ExecutionRepository executionRepository;

	/**
	 * Execute a remote script and returns an {@link Execution} object.
	 */
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@PostAuthorize("returnObject.username == authentication.name")
	@Transactional
	public Execution runScript(Long scriptId) throws RemoteExecutionException {
		Script script = findOne(scriptRepository, scriptId);

		// persist new execution
		Execution execution = new Execution(script);
		execution.setUsername(SecurityContextHolder.getContext().getAuthentication().getName());
		execution.setTimeStart(LocalDateTime.now());
		execution = executionRepository.save(execution);

		String command = String.format("sh %s %s >/dev/null 2>&1 & echo $!", script.getPath(), script.getParameters());
		String pid = executeCommand(script.getCredential(), command).trim();

		// update execution PID
		execution.setPid(pid);
		log.debug("Executed script: {}", execution);

		return executionRepository.save(execution);
	}

}
