/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.service.remoteexecution;

import static java.util.stream.Collectors.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.ciricuervo.rse.domain.Credential;
import org.ciricuervo.rse.domain.Server;
import org.ciricuervo.rse.repository.CredentialRepository;
import org.ciricuervo.rse.util.MultiValueMapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

/**
 * Provides functionality to monitor a {@link Server}.
 * 
 * @author ciri-cuervo
 */
@Service
@Transactional(readOnly = true)
@Slf4j
public class MonitorService extends RemoteExecutionService {

	private static final Pattern PROCESS_LINE_PATTERN = Pattern.compile("^(\\S+)\\s+(\\d+)\\s+");

	@Autowired
	private CredentialRepository credentialRepository;

	/**
	 * Returns the list of running processes in all servers.
	 */
	@PreAuthorize("hasAnyRole('ADMIN')")
	public Map<Server, List<String>> monitorServers() {
		Map<Server, List<String>> result = new HashMap<>();
		for (Credential credential : credentialRepository.findAll()) {
			try {
				MultiValueMapUtils.add(result, credential.getServer(), monitorServer(credential), LinkedList::new);
			} catch (RemoteExecutionException e) {
				log.error("Error monitoring server - {}", credential, e);
			}
		}
		return result;
	}

	/**
	 * Returns the list of running processes in a server for the given credential.
	 */
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	public String monitorServer(Long credentialId) throws RemoteExecutionException {
		return monitorServer(findOne(credentialRepository, credentialId));
	}

	/**
	 * Returns the list of running processes in a server for the given credential.
	 */
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	public String monitorServer(Credential credential) throws RemoteExecutionException {
		String command = String.format("ps auxf | grep --color=never \"^%s\\s\"", credential.getUser());
		String monitorOutput = executeCommand(credential, command);
		monitorOutput = removeCurrentProcess(monitorOutput, credential.getUser());
		return monitorOutput;
	}

	/**
	 * Returns the set of running PIDs in a server for the given credential.
	 */
	@PreAuthorize("hasAnyRole('SYSTEM', 'ADMIN', 'USER')")
	public Set<String> getRunningProcesses(Long credentialId) throws RemoteExecutionException {
		return getRunningProcesses(findOne(credentialRepository, credentialId));
	}

	/**
	 * Returns the set of running PIDs in a server for the given credential.
	 */
	@PreAuthorize("hasAnyRole('SYSTEM', 'ADMIN', 'USER')")
	public Set<String> getRunningProcesses(Credential credential) throws RemoteExecutionException {
		String monitorOutput = monitorServer(credential);
		String[] processes = monitorOutput.split("\n");
		return Arrays.stream(processes)
			.map(String::trim)
			.map(p -> getPid(p))
			.filter(Objects::nonNull)
			.collect(toSet());
	}

	private static String getPid(String processLine) {
		Matcher matcher = PROCESS_LINE_PATTERN.matcher(processLine);
		return matcher.find() ? matcher.group(2) : null;
	}

	private static String removeCurrentProcess(String output, String user) {
		String psLine = String.format("%s\\s+[^\\\\]+\\\\_ %%s\\s*", user);

		StringBuilder pattern = new StringBuilder(256);
		pattern.append("(?s)"); // (?s) -> flag to match newlines
		pattern.append(String.format(psLine, String.format("sshd: %s@notty", user)));
		pattern.append(String.format(psLine, String.format("bash -c ps auxf \\| grep --color=never \"\\^%s\\\\s\"", user)));
		pattern.append(String.format(psLine, "ps auxf"));
		pattern.append(String.format(psLine, String.format("grep --color=never \\^%s\\\\s", user)));

		log.debug("Remove pattern: {}", pattern);

		return output.replaceFirst(pattern.toString(), "");
	}

}
