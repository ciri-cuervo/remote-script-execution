/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.service.remoteexecution;

import lombok.Getter;

/**
 * Remote execution exception containing the error output.
 * 
 * @author ciri-cuervo
 */
@Getter
public class RemoteExecutionException extends Exception {

	private static final long serialVersionUID = 1321093262551262571L;

	private String output;

	public RemoteExecutionException(String message, Throwable cause) {
		super(message, cause);
	}

	public RemoteExecutionException(String message, String output) {
		super(message);
		this.output = output;
	}

	public RemoteExecutionException(Throwable cause) {
		super(cause);
	}

}
