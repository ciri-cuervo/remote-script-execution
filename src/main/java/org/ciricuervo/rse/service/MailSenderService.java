/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.service;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.mail.MessagingException;
import javax.mail.SendFailedException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

import org.ciricuervo.rse.domain.EmailHash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.thymeleaf.context.Context;
import org.thymeleaf.context.WebContext;
import org.thymeleaf.spring4.SpringTemplateEngine;

import lombok.extern.slf4j.Slf4j;

/**
 * The Thymeleaf email templates sender.
 * 
 * @author ciri-cuervo
 */
@Service
@Slf4j
public class MailSenderService {

	private static final int MAIL_SENDING_INTERVAL = 2000;

	@Autowired
	private Environment environment;
	@Autowired
	private JavaMailSender mailSender;
	@Autowired
	private SpringTemplateEngine templateEngine;
	@Autowired
	private MessageSourceService messageSourceService;
	@Autowired
	private RequestHelperService requestHelperService;

	private final BlockingQueue<MimeMessage> mailQueue = new LinkedBlockingQueue<>();

	private String from;

	@PostConstruct
	public void postConstruct() {
		initMailSenderThread();

		String mailUsername = environment.getProperty("spring.mail.username");
		if (StringUtils.hasText(mailUsername)) {
			if (mailUsername.contains("@")) {
				from = messageSourceService.getMessage("spring.mail.from", mailUsername);
			} else {
				from = mailUsername;
			}
		} else {
			log.info("Skip mail configuration: username not set.");
		}
	}

	@PreDestroy
	public void preDestroy() {
		try {
			// send poison object
			mailQueue.put(mailSender.createMimeMessage());
		} catch (InterruptedException e) {
			// ignore
		}
	}

	/**
	 * Send email.
	 */
	public void sendMail(String from, String to, String subject, String template, Map<String, ?> variables) throws MessagingException {

		// Prepare the evaluation context
		HttpServletRequest request = requestHelperService.getRequest();
		WebContext ctx = new WebContext(request, null, request.getServletContext(), LocaleContextHolder.getLocale(), variables);

		// Prepare message using a Spring helper
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		MimeMessageHelper message = new MimeMessageHelper(mimeMessage, false, "UTF-8");
		message.setFrom(from);
		message.setTo(to);
		message.setSubject(subject);

		// Create the HTML body using Thymeleaf
		String htmlContent = templateEngine.process(template, ctx);
		message.setText(htmlContent, true); // true = isHtml

		// Add message to mail queue
		try {
			mailQueue.put(mimeMessage);
		} catch (InterruptedException e) {
			throw new SendFailedException("Error sending `" + subject + "` to `" + to + "`", e);
		}
	}

	/**
	 * Send email with image.
	 */
	public void sendMailWithInline(String from, String to, String subject, String template, Map<String, ?> variables,
			String imageResourceName, byte[] imageBytes, String imageContentType) throws MessagingException {

		// Prepare the evaluation context
		Context ctx = new Context(LocaleContextHolder.getLocale(), variables);

		// Prepare message using a Spring helper
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, "UTF-8"); // true = multipart
		message.setFrom(from);
		message.setTo(to);
		message.setSubject(subject);

		// Create the HTML body using Thymeleaf
		String htmlContent = templateEngine.process(template, ctx);
		message.setText(htmlContent, true); // true = isHtml

		// Add the inline image, referenced from the HTML code as "cid:${imageResourceName}"
		InputStreamSource imageSource = new ByteArrayResource(imageBytes);
		message.addInline(imageResourceName, imageSource, imageContentType);

		// Add message to mail queue
		try {
			mailQueue.put(mimeMessage);
		} catch (InterruptedException e) {
			throw new SendFailedException("Error sending `" + subject + "` to `" + to + "`", e);
		}
	}

	/**
	 * Sends an email with a hash-URL in its content.
	 */
	public void sendHashMail(EmailHash emailHash, String subjectCode, String template) throws MessagingException {
		Map<String, Object> variables = new HashMap<>();
		variables.put("hash", emailHash.getHash());
		variables.put("baseUrl", requestHelperService.getBaseUrl());

		String to = emailHash.getEmail();
		String subject = messageSourceService.getMessage(subjectCode);
		this.sendMail(from, to, subject, template, variables);
	}

	private void initMailSenderThread() {
		new Thread("Mail Sender") {

			{
				setDaemon(true);
			}

			@Override
			public void run() {
				log.info("Email queue thread started.");
				while (true) {
					try {
						MimeMessage message = mailQueue.take();

						if (null == message.getFrom()) {
							// poison message makes thread to stop
							break;
						}

						mailSender.send(message);
						Thread.sleep(MAIL_SENDING_INTERVAL);

					} catch (Exception e) {
						log.error("Error sending message", e);
					}
				}
				log.info("Email queue thread stopped. Emails not sent: {}", mailQueue.size());
			}
		}.start();
	}

}
