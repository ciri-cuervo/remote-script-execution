/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.service;

import java.util.concurrent.ForkJoinPool;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.ciricuervo.rse.properties.ApplicationProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Global task-pool executor.
 * 
 * @author ciri-cuervo
 */
@Component
public class TaskPoolService {

	@Autowired
	private ApplicationProperties appProps;

	private ForkJoinPool forkJoinPool;

	@PostConstruct
	public void postConstruct() {
		forkJoinPool = new ForkJoinPool(appProps.getTaskParallelism());
	}

	@PreDestroy
	public void preDestroy() {
		forkJoinPool.shutdownNow();
	}

	public void execute(Runnable task) {
		forkJoinPool.execute(task);
	}

}
