/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;

import org.ciricuervo.rse.domain.User;
import org.ciricuervo.rse.properties.ApplicationProperties;
import org.ciricuervo.rse.util.DeletePathVisitor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.util.WebUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * Provides functionalities to manage system directories.
 * 
 * @author ciri-cuervo
 */
@Service
@Slf4j
public class PathManagerService {

	private static final String DATA_DIRNAME = "data";
	private static final String USERS_DIRNAME = "users";

	@Autowired
	private ServletContext servletContext;
	@Autowired
	private ApplicationProperties appProps;

	private Path temporalPath;
	private Path storagePath;
	private DeletePathVisitor deletePathVisitor = new DeletePathVisitor();

	@PostConstruct
	private void postConstruct() {
		// resolve temporal path
		if (appProps.getTemporalDir() != null) {
			temporalPath = Paths.get(appProps.getTemporalDir());
		} else {
			temporalPath = WebUtils.getTempDir(servletContext).toPath();
		}

		log.debug("Using temporal directory: {}", temporalPath);

		// resolve storage path
		if (appProps.getStorageDir() != null) {
			storagePath = Paths.get(appProps.getStorageDir());
		} else {
			storagePath = temporalPath.resolve(DATA_DIRNAME);
		}

		log.debug("Using storage directory: {}", storagePath);
	}

	/**
	 * Returns the users storage directory.
	 */
	public Path getUsersStoragePath() throws IOException {
		return Files.createDirectories(storagePath.resolve(USERS_DIRNAME));
	}

	/**
	 * Returns the storage directory of a user.
	 */
	public Path getUserStoragePath(User user) throws IOException {
		return Files.createDirectories(getUsersStoragePath().resolve(user.getId().toString()));
	}

	/**
	 * Deletes the storage directory of a user.
	 */
	public Path deleteUserStoragePath(User user) throws IOException {
		Path path = getUserStoragePath(user);
		log.info("Delete user storage directory [id={}, username={}, path={}]", user.getId(), user.getUsername(), path);
		return Files.walkFileTree(path, deletePathVisitor);
	}

}
