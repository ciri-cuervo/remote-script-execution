/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.service;

import org.ciricuervo.rse.domain.EmailHash;
import org.ciricuervo.rse.domain.User;
import org.ciricuervo.rse.domain.type.AuthorityType;
import org.ciricuervo.rse.domain.type.EmailHashType;
import org.ciricuervo.rse.domain.type.UserStatusType;
import org.ciricuervo.rse.repository.EmailHashRepository;
import org.ciricuervo.rse.repository.UserRepository;
import org.ciricuervo.rse.web.command.HashCommand;
import org.ciricuervo.rse.web.command.PasswordResetConfirmCommand;
import org.ciricuervo.rse.web.command.SignupCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Provides functionality to manage user accounts.
 * 
 * @author ciri-cuervo
 */
@Service
@Transactional
public class AccountService {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private EmailHashRepository emailHashRepository;
	@Autowired
	private EmailHashService emailHashService;

	/**
	 * Create new user account.
	 */
	public EmailHash createUser(SignupCommand signupCommand) {
		User user = new User();
		user.setUsername(signupCommand.getUsername());
		user.setEmail(signupCommand.getEmail());
		user.setPassword(new BCryptPasswordEncoder().encode(signupCommand.getPassword()));
		user.addAuthority(AuthorityType.ROLE_USER);
		user.setStatus(UserStatusType.WAITING_ACTIVATION);
		user = userRepository.save(user);
		return emailHashService.createEmailHash(signupCommand.getEmail(), EmailHashType.ACCOUNT_ACT);
	}

	/**
	 * Activate user account.
	 */
	public void activateAccount(HashCommand command) {
		EmailHash emailHash = emailHashRepository.findByHash(command.getHash()).get();
		User user = userRepository.findByEmail(emailHash.getEmail()).get();
		user.setStatus(UserStatusType.ACTIVE);
		user.setLoginAttempts(0);
		user = userRepository.save(user);
		emailHashRepository.deactivateByEmail(emailHash.getEmail());
	}

	/**
	 * Unlock user account.
	 */
	public void unlockAccount(HashCommand command) {
		activateAccount(command);
	}

	/**
	 * Change user password.
	 */
	public void changePassword(PasswordResetConfirmCommand command) {
		EmailHash emailHash = emailHashRepository.findByHash(command.getHash()).get();
		User user = userRepository.findByEmail(emailHash.getEmail()).get();
		user.setPassword(new BCryptPasswordEncoder().encode(command.getPassword()));
		user = userRepository.save(user);
		emailHashRepository.deactivateByEmail(emailHash.getEmail());
	}

}
