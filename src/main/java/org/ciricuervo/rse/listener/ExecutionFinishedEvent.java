/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.listener;

import org.ciricuervo.rse.domain.Execution;
import org.springframework.context.ApplicationEvent;
import org.springframework.util.Assert;

import lombok.Getter;

/**
 * Event that carries an {@link Execution}.
 * 
 * @author ciri-cuervo
 */
@Getter
public class ExecutionFinishedEvent extends ApplicationEvent {

	private static final long serialVersionUID = 2484896155608429851L;

	private final Execution execution;

	/**
	 * Create a new Event.
	 * 
	 * @param source the object on which the event initially occurred (never {@code null})
	 * @param execution the {@link Execution} object (never {@code null})
	 */
	public ExecutionFinishedEvent(Object source, Execution execution) {
		super(source);
		Assert.notNull(execution, "Execution must not be null");
		this.execution = execution;
	}

}
