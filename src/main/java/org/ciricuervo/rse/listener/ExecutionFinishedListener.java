/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.listener;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

import javax.transaction.Transactional;

import org.ciricuervo.rse.domain.Execution;
import org.ciricuervo.rse.repository.ExecutionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

/**
 * Listener that handles {@link ExecutionFinishedEvent}.
 * 
 * @author ciri-cuervo
 */
@Service
@Slf4j
public class ExecutionFinishedListener {

	@Autowired
	private ExecutionRepository executionRepository;
	@Autowired
	private SimpMessagingTemplate messagingTemplate;

	/**
	 * Updates {@link Execution} by setting the end-time.
	 */
	@EventListener
	@Transactional
	public void updateExecutionFinished(ExecutionFinishedEvent event) {
		Execution execution = event.getExecution();
		LocalDateTime time = LocalDateTime.ofEpochSecond(event.getTimestamp(), 0, ZoneOffset.UTC);
		execution.setTimeEnd(time);
		log.debug("Update end time - {}", execution);
		executionRepository.save(execution);
	}

	/**
	 * Sends a web-socket notification to user that produced the execution.
	 */
	@EventListener(condition = "#event.execution.username != null")
	public void sendUserNotification(ExecutionFinishedEvent event) {
		Execution execution = event.getExecution();
		log.debug("Send web socket notification - {}", execution);
		messagingTemplate.convertAndSendToUser(execution.getUsername(), "/topic/execution", execution);
	}

}
