/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.repository;

import java.util.Optional;

import org.ciricuervo.rse.domain.Server;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * {@link Server} repository.
 * 
 * @author ciri-cuervo
 */
@Repository
@RepositoryRestResource
@Transactional(readOnly = true)
public interface ServerRepository extends JpaRepository<Server, Long> {

	Optional<Server> findByName(String name);

}
