/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.repository;

/**
 * Custom {@link org.ciricuervo.rse.domain.EmailHash EmailHash} repository.
 * 
 * @author ciri-cuervo
 */
public interface EmailHashRepositoryCustom {

	Integer deactivateByEmail(String email);

	Integer deactivateByExpired();

}
