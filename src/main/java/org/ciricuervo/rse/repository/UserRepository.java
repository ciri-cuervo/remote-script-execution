/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.repository;

import java.time.LocalDateTime;
import java.util.Optional;

import org.ciricuervo.rse.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * {@link User} repository.
 * 
 * @author ciri-cuervo
 */
@Repository
@RepositoryRestResource
@Transactional(readOnly = true)
public interface UserRepository extends JpaRepository<User, Long> {

	Optional<User> findByUsername(String username);

	Optional<User> findByEmail(String email);

	@Transactional
	@Modifying
	@Query("delete User u where u.status = 'WAITING_ACTIVATION' and u.createdDate <= ?1")
	Integer deleteByWaitingActivation(LocalDateTime createdDate);

}
