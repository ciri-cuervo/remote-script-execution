/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.repository;

import javax.persistence.Query;

import org.ciricuervo.rse.domain.EmailHash;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Custom {@link EmailHash} repository implementation.
 * 
 * @author ciri-cuervo
 */
@Repository
@Transactional
public class EmailHashRepositoryImpl extends CustomRepositoryImpl implements EmailHashRepositoryCustom {

	@Override
	public Integer deactivateByEmail(String email) {
		String hsql = "update EmailHash e set e.active = false where e.active = true and e.email = :email";
		Query query = entityManager.createQuery(hsql);
		query.setParameter("email", email);
		return executeUpdate(query);
	}

	@Override
	public Integer deactivateByExpired() {
		String hsql = "update EmailHash e set e.active = false where e.active = true and e.expire <= NOW()";
		Query query = entityManager.createQuery(hsql);
		return executeUpdate(query);
	}

}
