/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * Custom modifying repository.
 * 
 * @author ciri-cuervo
 */
public abstract class CustomRepositoryImpl {

	@PersistenceContext
	protected EntityManager entityManager;

	protected Integer executeUpdate(Query query) {
		entityManager.flush();
		Integer result = query.executeUpdate();
		entityManager.clear();
		return result;
	}

}
