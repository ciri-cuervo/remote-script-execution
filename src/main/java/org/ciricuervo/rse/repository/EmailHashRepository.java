/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.repository;

import java.util.Optional;

import org.ciricuervo.rse.domain.EmailHash;
import org.ciricuervo.rse.domain.type.EmailHashType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * {@link EmailHash} repository.
 * 
 * @author ciri-cuervo
 */
@Repository
@Transactional(readOnly = true)
public interface EmailHashRepository extends JpaRepository<EmailHash, Long>, EmailHashRepositoryCustom {

	Optional<EmailHash> findByHash(String hash);

	Optional<EmailHash> findByHashAndActiveTrue(String hash);

	Optional<EmailHash> findByEmailAndTypeAndActiveTrue(String email, EmailHashType type);

	@Transactional
	@Modifying
	Integer deleteByActiveFalse();

}
