/**
 * Copyright (C) 2016 ciri-cuervo [http://bitbucket.org/ciri-cuervo, http://github.com/ciri-cuervo] -All Rights Reserved-
 */
package org.ciricuervo.rse.repository;

import org.ciricuervo.rse.domain.Credential;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * {@link Credential} repository.
 * 
 * @author ciri-cuervo
 */
@Repository
@RepositoryRestResource
@Transactional(readOnly = true)
public interface CredentialRepository extends JpaRepository<Credential, Long> {

}
