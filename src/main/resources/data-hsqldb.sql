INSERT INTO user (id, username, password, email, firtname, lastname, address, phone, info, dob, login_attempts, status, created_date, last_modified_date) VALUES
(1, 'system', '$2a$10$yi0CJZSlLxo6PUPBoxon2.qfW05gtElOqAyAFS.A.5m8149gkn9GK', 'no-reply@email.com', NULL, NULL, NULL, NULL, NULL, NULL, 0, 'ACTIVE', NOW(), NOW());
INSERT INTO user (id, username, password, email, firtname, lastname, address, phone, info, dob, login_attempts, status, created_date, last_modified_date) VALUES
(2, 'admin', '$2a$10$yi0CJZSlLxo6PUPBoxon2.qfW05gtElOqAyAFS.A.5m8149gkn9GK', 'admin@email.com', NULL, NULL, NULL, NULL, NULL, NULL, 0, 'ACTIVE', NOW(), NOW());
INSERT INTO user (id, username, password, email, firtname, lastname, address, phone, info, dob, login_attempts, status, created_date, last_modified_date) VALUES
(3, 'user', '$2a$10$yi0CJZSlLxo6PUPBoxon2.qfW05gtElOqAyAFS.A.5m8149gkn9GK', 'user@email.com', NULL, NULL, NULL, NULL, NULL, NULL, 0, 'ACTIVE', NOW(), NOW());

INSERT INTO user_authorities (user_id, authorities) VALUES
(1, 'ROLE_SYSTEM');
INSERT INTO user_authorities (user_id, authorities) VALUES
(2, 'ROLE_ADMIN');
INSERT INTO user_authorities (user_id, authorities) VALUES
(3, 'ROLE_USER');

INSERT INTO server (id, ip, host, name, protocol, port, created_date, last_modified_date) VALUES
(1, '127.0.0.1', 'localhost', 'ciri-VirtualBox', 'ssh', '3022', NOW(), NOW());
INSERT INTO server (id, ip, host, name, protocol, port, created_date, last_modified_date) VALUES
(2, '127.0.1.1', 'remotehost', 'ciri-Invalid', 'ssh', '3022', NOW(), NOW());

INSERT INTO credential (id, server_id, user, pass, created_date, last_modified_date) VALUES
(1, 1, 'ciri', 'ciri', NOW(), NOW());
INSERT INTO credential (id, server_id, user, pass, created_date, last_modified_date) VALUES
(2, 2, 'ciri', 'ciri', NOW(), NOW());

INSERT INTO script (id, name, description, path, parameters, credential_id, created_date, last_modified_date) VALUES
(1, 'script 1', 'description 1', '~/scripts/script1.sh', 'PARAM1 PARAM2', 1, NOW(), NOW());
INSERT INTO script (id, name, description, path, parameters, credential_id, created_date, last_modified_date) VALUES
(2, 'script 2', 'description 2', '~/scripts/script2.sh', 'PARAM1 PARAM2', 1, NOW(), NOW());
