(function(JS) {

	var bindRepeatPasswordValidation = function() {
		$('form input[name=password]').on('change input', function(e) {
			if (this.checkValidity()) {
				$('form input[name=repeatPassword]').attr('pattern', this.value);
			}
		});

		// trigger change to get pattern after browser auto-complete
		setTimeout(function () {
			$('form input[name=password]').change();
		}, 1000);
	};

	var bindCleanInputsAfterChange = function() {
		$('form div.form-group.has-error').find('input, select, textarea').on('change input', function(e) {
			$(this).closest('div.form-group.has-error').removeClass('has-error');
		});
	};

	var fixAutofocusOnError = function() {
		$('form div.form-group.has-error:first').find('input, select, textarea').first().focus();
	};

	var fixMozErrorMessage = function() {
		$('form').find('input[title], select[title], textarea[title]').each(function() {
			$(this).attr('x-moz-errormessage', $(this).attr('title'))
		});
	};

	var bindLogoutBtnAction = function() {
		$('#logout').on('click', 'a', function(e) {
			e.preventDefault();
			$(this).siblings('form').submit();
		});
	};

	$(function() {
		bindRepeatPasswordValidation();
		bindCleanInputsAfterChange();
		fixAutofocusOnError();
		fixMozErrorMessage();
		bindLogoutBtnAction();
	});

})(JS);
