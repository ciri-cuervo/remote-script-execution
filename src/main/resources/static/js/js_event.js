var LogoutEvent = function() {

	var handleEvent = function() {
		$('#logout').hide();
	};

	return {
		handle : function() {
			handleEvent();
			return this;
		}
	}
};
