(function(JS) {

	var createWebsocketConn = function() {
		var socket = new SockJS(JS.contextPath + '/websocket');

		var headers = {};
		headers[JS.csrf.headerName] = JS.csrf.token;

		var client = Stomp.over(socket);
		client.debug = null;
		client.connect(headers, function(frame) {
			console.log('Connected: ' + frame);

			// subscribe to specific user messages
			client.subscribe("/user/topic/execution", function(message) {
				var execution = JSON.parse(message.body);
				new ExecutionNotification(execution).show();
			});
			client.subscribe("/user/topic/logout", function(message) {
				new LogoutEvent().handle();
			});

		}, function(error) {
			// error callback, display the error message:
			console.log(error.headers ? error.headers.message : error);

			if (JS.websocket.reconnect) {
				if (authenticationError(error)) {
					// don't reconnect, user must refresh page
				} else {
					// try to reconnect
					setTimeout(createWebsocketConn, 1000);
				}
			}
		});
	};

	var authenticationError = function(error) {
		return error.headers && error.headers.message.indexOf('InvalidCsrfTokenException') > -1;
	};

	$(function() {
		createWebsocketConn();
	});

})(JS);
