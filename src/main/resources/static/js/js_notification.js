var ExecutionNotification = function(execution) {

	var $notificationDiv;

	var showMessage = function() {
		if ($notificationDiv) {
			$notificationDiv.show();
		} else {
			// TODO parse message
			var message = JSON.stringify(execution);
			$notificationDiv = $('<div></div>').text(message);
			$('#notifications').append($notificationDiv);
		}
	};

	var hideMessage = function() {
		$notificationDiv && $notificationDiv.hide();
	};

	return {
		show : function() {
			showMessage();
			return this;
		},
		hide : function() {
			hideMessage();
			return this;
		}
	}
};
